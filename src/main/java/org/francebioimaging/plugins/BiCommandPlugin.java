package org.francebioimaging.plugins;

import java.io.File;
import java.io.IOException;

import org.francebioimaging.runner.BiRunnerContainer;
import org.francebioimaging.runner.BiRunnerModel;
import org.scijava.command.Command;
import org.scijava.plugin.Parameter;
import org.scijava.plugin.Plugin;
import org.scijava.ui.UIService;

import io.scif.services.DatasetIOService;
import net.imagej.ImageJ;
import net.imagej.display.ImageDisplayService;

@Plugin(type = Command.class, headless = true,
menuPath = "BioImageJ>BiCommand")
public class BiCommandPlugin implements Command {

	@Parameter
	File xml;
	
	@Parameter
	String args;
	
	@Parameter
	ImageDisplayService imDispService;
	
	@Parameter
	DatasetIOService ioService;

	@Parameter
	UIService uiService;
	
	@Override
	public void run() {
		
		BiRunnerContainer runnerContainer = new BiRunnerContainer();
		runnerContainer.setImageDisplayService(imDispService);
		runnerContainer.setIOService(ioService);
		runnerContainer.setUIService(uiService);
		BiRunnerModel runnerModel = new BiRunnerModel(runnerContainer);
		runnerModel.setTmpDir(BiSettingsSingleton.getInstance().getValue("BioImageJ", "Temporary folder"));
		
		// run sequence
		try {
			runnerContainer.setProcessFile(xml.getCanonicalPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		runnerModel.load();
		runnerContainer.getProcessInfo().setArgs(args);
		runnerModel.run();
		
	}
	
	public static void main(final String[] args) {
		final ImageJ ij = new ImageJ();
		ij.launch(args);
	}
}
