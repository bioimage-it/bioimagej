package org.francebioimaging.plugins;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import org.francebioimaging.browser.BiBrowserComponent;
import org.francebioimaging.browser.BiBrowserContainer;
import org.francebioimaging.browser.BiBrowserModel;
import org.francebioimaging.framework.BiComponent;
import org.francebioimaging.framework.BiContainer;
import org.scijava.command.CommandService;
import org.scijava.ui.UIService;

import io.scif.services.DatasetIOService;
import net.imagej.display.ImageDisplayService;


public class BiBrowser extends BiComponent {

	protected JFrame frame;
	protected CommandService cmd;
	
	protected ImageDisplayService imDispService;
	protected DatasetIOService ioService;
	protected UIService uiService;
	protected BiBrowserContainer browserContainer;

	public BiBrowser(String categoriesJsonFile, String processesDir) {
		browserContainer = new BiBrowserContainer();
		browserContainer.addObserver(this);
		BiBrowserModel browserModel = new BiBrowserModel(browserContainer);
		browserModel.init();
		BiBrowserComponent browserComponent = new BiBrowserComponent(browserContainer);

		// initialize
		browserContainer.setCategoriesJsonFile(categoriesJsonFile);
		browserContainer.setProcessesDir(processesDir);
		browserContainer.notify(BiBrowserContainer.ProcessesDirChanged);

		// display JFrame
		frame = new JFrame();
		//BiStylesheet.defaultBackground(frame);
		frame.setBounds(100, 100, 1200, 600);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(browserComponent.getToolBar(), BorderLayout.PAGE_START);
		frame.getContentPane().add(browserComponent.getCentralWidget(), BorderLayout.CENTER);
	}
	
	public void update(BiContainer container, String action) {
		if (action == BiBrowserContainer.OpenProcess) {
			System.out.println("BiToolsBrowser open");
			
			String processType = "ext";
			if ( processType == "ImageJ" ) {
				// add here fiji code to open a plugin
				cmd.run("org.scijava.plugins.commands.io.OpenFile", true);
			}
			else {
				BiRunner runner = new BiRunner(browserContainer.getClickedProcess().getXmlFileUrl());
				runner.setImageDisplayService(imDispService);
				runner.setIOService(ioService);
				runner.setUIService(uiService);
				runner.getFrame().setVisible(true);
			}
			
		}
	}
	
	public void setImageDisplayService(ImageDisplayService  imDisp) {
		this.imDispService = imDisp;
	}
	
	public void setIOService(DatasetIOService ioService) {
		this.ioService = ioService;
	}
	
	public void setUIService(UIService uiService) {
		this.uiService = uiService;
	}
	
	public void setCommandService(CommandService cmd) {
		this.cmd = cmd;
	}
	
	public JFrame getFrame() {
		return frame;
	}

}
