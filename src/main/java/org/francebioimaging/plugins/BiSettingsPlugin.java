package org.francebioimaging.plugins;

import org.scijava.command.Command;
import org.scijava.plugin.Plugin;

import net.imagej.ImageJ;

@Plugin(type = Command.class, headless = true,
menuPath = "BioImageJ>Settings")
public class BiSettingsPlugin implements Command {

	protected BiSettings settings;
	
	@Override
	public void run() {
		
		if (settings == null) {
			settings = new BiSettings();
		}
		settings.getFrame().setVisible(true);
		
	}
	
	public static void main(final String[] args) {
		final ImageJ ij = new ImageJ();
		ij.launch(args);
	}

}
