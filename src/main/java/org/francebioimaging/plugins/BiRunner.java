package org.francebioimaging.plugins;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import org.francebioimaging.framework.BiComponent;
import org.francebioimaging.framework.BiContainer;
import org.francebioimaging.runner.BiRunnerComponent;
import org.francebioimaging.runner.BiRunnerContainer;
import org.francebioimaging.runner.BiRunnerModel;
import org.scijava.ui.UIService;

import io.scif.services.DatasetIOService;
import net.imagej.display.ImageDisplayService;

public class BiRunner extends BiComponent{

	protected JFrame frame;
	protected BiRunnerContainer runnerContainer;
	protected BiRunnerModel runnerModel;
	
	
	public BiRunner(String xmlFile) {
		objectName = "BiRunner";

		// components
		runnerContainer = new BiRunnerContainer();
		
		runnerModel = new BiRunnerModel(runnerContainer);
		runnerModel.setTmpDir(BiSettingsSingleton.getInstance().getValue("BioImageJ", "Temporary folder"));
		BiRunnerComponent runnerComponent = new BiRunnerComponent(runnerContainer);

		//runnerContainer.printObservers();
		
		// initialize
		runnerContainer.addObserver(this);
		runnerContainer.setProcessFile(xmlFile);
		runnerContainer.notify(BiRunnerContainer.ProcessFileChanged);

		// Frame
		frame = new JFrame();
		frame.setBounds(100, 100, 350, 600);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(runnerComponent.getWidget(), BorderLayout.CENTER);
	}
	
	public void setImageDisplayService(ImageDisplayService  imDisp) {
		runnerContainer.setImageDisplayService(imDisp);
	}
	
	public void setIOService(DatasetIOService ioService) {
		runnerContainer.setIOService(ioService);
	}
	
	public void setUIService(UIService uiService) {
		runnerContainer.setUIService(uiService);
	}
	
	public void update(BiContainer container, String action) {
		if (action.equals( BiRunnerContainer.CancelClicked )) {
			frame.setVisible(false);
			frame.dispose();
		}
	}
	
	public JFrame getFrame() {
		return frame;
	}
	
}
