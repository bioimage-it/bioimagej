package org.francebioimaging.plugins;

import org.scijava.app.StatusService;
import org.scijava.command.Command;
import org.scijava.command.CommandService;
import org.scijava.log.LogService;
import org.scijava.plugin.Parameter;
import org.scijava.plugin.Plugin;
import org.scijava.thread.ThreadService;
import org.scijava.ui.UIService;

import io.scif.services.DatasetIOService;
import net.imagej.ImageJ;
import net.imagej.display.ImageDisplayService;
import net.imagej.ops.OpService;

@Plugin(type = Command.class, headless = true,
menuPath = "BioImageJ>Runner")
public class BiRunnerPlugin implements Command {

	@Parameter
	String xmlFile;
	
	@Parameter
	ImageDisplayService imDispService;
	
	@Parameter
	DatasetIOService ioService;

	@Parameter
	OpService ops;

	@Parameter
	LogService log;

	@Parameter
	UIService uiService;

	@Parameter
	CommandService cmd;

	@Parameter
	StatusService status;

	@Parameter
	ThreadService thread;
	
	@Override
	public void run() {
		
		// get the image list
		/*
		List<ImageDisplay> list = imDispService.getImageDisplays(); // getActiveImageDisplay()
		List<String> imagesNames = new ArrayList<String>();
		for (int i = 0 ; i < list.size() ; i++) {
			String id = list.get(i).getName();
			imagesNames.add(list.get(i).getName());
			System.out.println(id);
		}
		*/
		
		// create the graphical user interface
		BiRunner runner = new BiRunner(xmlFile);
		runner.setImageDisplayService(imDispService);
		runner.setIOService(ioService);
		runner.setUIService(uiService);
		runner.getFrame().setVisible(true);
				
	}


	public static void main(final String[] args) {
		final ImageJ ij = new ImageJ();
		ij.launch(args);
	}

}
