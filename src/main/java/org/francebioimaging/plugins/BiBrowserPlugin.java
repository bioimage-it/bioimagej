package org.francebioimaging.plugins;

import net.imagej.ImageJ;
import net.imagej.display.ImageDisplayService;
import net.imagej.ops.OpService;

import org.scijava.app.StatusService;
import org.scijava.command.Command;
import org.scijava.command.CommandService;
import org.scijava.log.LogService;
import org.scijava.plugin.Parameter;
import org.scijava.plugin.Plugin;
import org.scijava.thread.ThreadService;
import org.scijava.ui.UIService;

import io.scif.services.DatasetIOService;

@Plugin(type = Command.class, headless = true,
menuPath = "BioImageJ>Browser")
public class BiBrowserPlugin implements Command {

	@Parameter
	OpService ops;

	@Parameter
	LogService log;

	@Parameter
	CommandService cmd;

	@Parameter
	StatusService status;

	@Parameter
	ThreadService thread;
	
	@Parameter
	DatasetIOService ioService;
	
	@Parameter
	UIService uiService;
	
	@Parameter
	ImageDisplayService imDispService;

	private static BiBrowser browser = null;


	/**
	 * show a dialog and give the dialog access to required IJ2 Services
	 */
	@Override
	public void run() {

		if (browser == null) {
			
			String categoriesFile = BiSettingsSingleton.getInstance().getValue("BioImageJ", "Categories json");
			String processesDir = BiSettingsSingleton.getInstance().getValue("BioImageJ", "Processes directory");
			
			//System.out.println("categoriesFile: " + categoriesFile);
			//System.out.println("processesDir: " + processesDir);
			
			browser = new BiBrowser(categoriesFile, processesDir);
			browser.setCommandService(cmd);
			browser.setImageDisplayService(imDispService);
			browser.setIOService(ioService);
			browser.setUIService(uiService);
		}
		browser.getFrame().setVisible(true);

	}

	public static void main(final String[] args) {
		final ImageJ ij = new ImageJ();
		ij.launch(args);
	}
}
