package org.francebioimaging.plugins;

import java.io.File;
import java.nio.file.Paths;

import org.francebioimaging.settings.BiSettingInfo;
import org.francebioimaging.settings.BiSettingsGroup;
import org.francebioimaging.settings.BiSettingsParser;
import org.francebioimaging.settings.BiSettingsSet;
import org.francebioimaging.settings.BiSettingsTypes;

public class BiSettingsSingleton {

	private BiSettingsSingleton(){

	}

	private static BiSettingsSet INSTANCE = null;

	public static BiSettingsSet getInstance()
	{           
		if (INSTANCE == null){   

			// search the settings file in the default location. Create it if not exists.
			String pluginsDir = Paths.get(System.getProperty("user.dir"), "plugins").toString();
			//System.out.println("pluginsDir = " + pluginsDir);

			File pluginsDirFile = new File(pluginsDir);
			if (pluginsDirFile.exists()) {
				String settingsPath = Paths.get(pluginsDir, "bioimagej", "settings.json").toString();
				File settingsPathFile = new File(settingsPath);
				if (!settingsPathFile.exists()) {
					initializeSettings(Paths.get(pluginsDir, "bioimagej").toString(), settingsPath);
				}
				
				BiSettingsParser parser = new BiSettingsParser(settingsPath);
				INSTANCE = parser.parse();

			}
			else {
				System.out.println("plugins/ does not exists");
			}
			
		}
		return INSTANCE;
	}

	public static void initializeSettings(String baseDir, String filePath) {
		BiSettingsSet settingsSet = new BiSettingsSet();

		BiSettingsGroup settingsGroup = new BiSettingsGroup();
		settingsGroup.setName("BioImageJ");

		// categories
		BiSettingInfo settingCategories = new BiSettingInfo();
		settingCategories.setKey("Categories json");
		settingCategories.setType(BiSettingsTypes.FILE);
		settingCategories.setValue(Paths.get(baseDir, "categories", "categories.json").toString());
		settingsGroup.addSetting(settingCategories);

		// Processes directory
		BiSettingInfo settingProcesses = new BiSettingInfo();
		settingProcesses.setKey("Processes directory");
		settingProcesses.setType(BiSettingsTypes.DIR);
		settingProcesses.setValue(Paths.get(baseDir, "processes").toString());
		settingsGroup.addSetting(settingProcesses);

		// Temporary folder
		BiSettingInfo settingTmp = new BiSettingInfo();
		settingTmp.setKey("Temporary folder");
		settingTmp.setType(BiSettingsTypes.DIR);
		settingTmp.setValue(Paths.get(baseDir, "tmp").toString());
		settingsGroup.addSetting(settingTmp);

		settingsSet.addGroup(settingsGroup);

		BiSettingsParser parser = new BiSettingsParser(filePath);
		parser.save(settingsSet);

	}
}
