package org.francebioimaging.plugins;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import org.francebioimaging.settings.BiSettingsComponent;
import org.francebioimaging.settings.BiSettingsContainer;
import org.francebioimaging.settings.BiSettingsModel;

public class BiSettings {

	
	protected BiSettingsContainer settingsContainer;
	protected JFrame frame;
	
	public BiSettings() {
		
		// components
		settingsContainer = new BiSettingsContainer();
		BiSettingsModel settingsModel = new BiSettingsModel(settingsContainer);
		settingsModel.init();
		BiSettingsComponent settingsComponent = new BiSettingsComponent(settingsContainer);
		
		// initialization
		settingsContainer.setSettingsFile(BiSettingsSingleton.getInstance().getJsonFile());
		settingsContainer.setSettingsSet(BiSettingsSingleton.getInstance());
		settingsContainer.notify(BiSettingsContainer.SettingsLoaded);
		
		// JFrame
		frame = new JFrame();
		frame.setBounds(100, 100, 900, 300);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(settingsComponent.getWidget(), BorderLayout.CENTER);
		
	}
	
	
	public JFrame getFrame() {
		return frame;
	}
	
}
