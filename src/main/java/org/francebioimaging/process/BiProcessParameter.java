package org.francebioimaging.process;

import org.francebioimaging.runner.BiCmdSelect;

public class BiProcessParameter {
	
    String name; // str: parameter name
    String description; // str: Parameter description
    String value; // str: Parameter value
    String type; // str: parameter type (in PARAM_XXX names)
    boolean isData; // bool: False if parameter is param and True if parameter is data
    String io; // str: IO type if parameter is IO (in IO_XXX names)
    String defaultValue; // str: Parameter default value
    BiCmdSelect selectInfo; // BiCmdSelect: Choices for a select parameter
    String valueSuffix; // str: Parameter suffix (needed if programm add suffix to IO)
    boolean isAdvanced; // bool: True if parameter is advanced
	
    public BiProcessParameter() {
        name = "";
        description = "";
        value = "";
        type = "";
        isData = false;
        io = "";
        defaultValue = "";
        selectInfo = new BiCmdSelect();
        valueSuffix = "";
        isAdvanced = false ;
    }
    
    public void display() {
        System.out.println("\tname:"+ name); 
        System.out.println("\tdescription:"+ description);  
        System.out.println("\tvalue:"+ value);  
        System.out.println("\ttype:"+ type);
        System.out.println("\tio:"+ io); 
        System.out.println("\tdefault_value:"+ defaultValue); 
        System.out.println("\tvalue_suffix:"+ valueSuffix);
        System.out.println("\tis_advanced:"+ isAdvanced);
        System.out.println("\t------------");
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isData() {
		return isData;
	}

	public void setData(boolean isData) {
		this.isData = isData;
	}

	public String getIo() {
		return io;
	}

	public void setIo(String io) {
		this.io = io;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public BiCmdSelect getSelectInfo() {
		return selectInfo;
	}

	public void setSelectInfo(BiCmdSelect selectInfo) {
		this.selectInfo = selectInfo;
	}

	public String getValueSuffix() {
		return valueSuffix;
	}

	public void setValueSuffix(String valueSuffix) {
		this.valueSuffix = valueSuffix;
	}

	public boolean isAdvanced() {
		return isAdvanced;
	}

	public void setAdvanced(boolean isAdvanced) {
		this.isAdvanced = isAdvanced;
	}
}
