package org.francebioimaging.process;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.francebioimaging.core.BiTypes;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class BiProcessParser {

	BiProcessInfo info;
	String xmlFileUrl;
	Element root;

	public BiProcessParser(String xmlFileUrl) {
		info = new BiProcessInfo();
		this.xmlFileUrl = xmlFileUrl;
		info.setXmlFileUrl(xmlFileUrl);
	}

	/*
	 * Parse the XML file
	 *
	 * Returns
	 * -------
	 * BiProcessInfo
	 *     The process information extracted from the XML file    
	 */
	public BiProcessInfo parse() {    

		this.readXmlFile();

		if ( !root.getNodeName().equals("tool") ){
			System.out.println("The process xml file must contains a <tool> root tag");
			//throw new BiProcessParseException("The process XML file must contains a <tool> root tag");
		}

		this.parseTool();
		final NodeList childs = root.getChildNodes();

		for (int i = 0; i<childs.getLength(); i++) 
		{
			if ( !childs.item(i).getNodeName().equals("#text") ) {
				final Element child = (Element) childs.item(i);
	
				if ( child.getNodeName().equals("description") ){
					String desc = child.getTextContent().replace("\t", "");
					info.setDescription(desc); 
				}
				else if (child.getNodeName().equals("command") ){
					this.parseCommand(child);
				}
				else if ( child.getNodeName().equals("inputs") ){
					this.parseInputs(child);
				}
				else if ( child.getNodeName().equals("outputs") ){
					this.parseOutputs(child);
				}
				else if ( child.getNodeName().equals("help") ){
					this.parseHelp(child);
				}
				else if ( child.getNodeName().equals("categories") ){
					this.parseCategories(child);
				}
			}
		}        

		this.parseHiddenParams();
		return this.info;
	}

	private void readXmlFile() {

		try 
		{
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder builder = factory.newDocumentBuilder();		
			final Document document= builder.parse(new File(xmlFileUrl));
			root = document.getDocumentElement();
		}
		catch (final ParserConfigurationException e) 
		{
			e.printStackTrace();
		}
		catch (final SAXException e) 
		{
			e.printStackTrace();
		}
		catch (final IOException e) 
		{
			e.printStackTrace();
		}
	}

	// Parse the tool information"""
	private void parseTool() {

		this.info.setId(this.root.getAttribute("id"));
		this.info.setName(this.root.getAttribute("name"));
		this.info.setVersion(this.root.getAttribute("version"));
		this.info.setType(this.root.getAttribute("type"));

	}

	// Parse the tool command"""
	private void parseCommand(Element node) {

		String command = node.getTextContent();
		command = command.replace("\t", "");
		command = command.replace("\n", "");
		this.info.setCommand(command); 
		String[] args = command.split(" ");
		Vector<String> commandArgs = new Vector<String>(Arrays.asList(args));
		this.info.setCommandArgs(commandArgs);

	}

	// Parse the help information"""
	private void parseHelp(Element node) {

		this.info.setHelp(node.getAttribute("url"));

	}

	// Parse categories"""
	private void parseCategories(Element node) {

		final NodeList childs = node.getChildNodes();
		for (int i = 0; i<childs.getLength(); i++) 
		{
			if ( !childs.item(i).getNodeName().equals("#text")) {
				final Element child = (Element) childs.item(i);
				if (child.getNodeName().equals("category")) {
					this.info.addCategory(child.getTextContent());
				}
			}
		}
	}

	// Parse the inputs"""
	private void parseInputs(Element node) {

		final NodeList childs = node.getChildNodes();
		for (int i = 0; i<childs.getLength(); i++) 
		{
			if ( !childs.item(i).getNodeName().equals("#text")) {
				final Element child = (Element) childs.item(i);
	
				if ( child.getNodeName().equals("param") ){
					BiProcessParameter input_parameter = new BiProcessParameter();
					input_parameter.setIo(BiTypes.IO_PARAM());
					input_parameter.setData(false);
	
					input_parameter.setName(child.getAttribute("name"));
					input_parameter.setDescription(child.getAttribute("label"));
	
					String type = child.getAttribute("type");
					if ( type.equals("number") ) {
						input_parameter.setType(BiTypes.PARAM_NUMBER());
					}
					else if ( type.equals("string") ) {
						input_parameter.setType(BiTypes.PARAM_STRING());
					}
					else if ( type.equals("bool") || type.equals("boolean") ) {
						input_parameter.setType(BiTypes.PARAM_BOOLEAN());
					}
					else {
						System.out.println("The format of the input param " + input_parameter.name + " is not supported");
					}
	
					input_parameter.setValue(child.getAttribute("value"));
					if ( child.getAttribute("advanced").equals("True") || child.getAttribute("advanced").equals("true")) {
						input_parameter.setAdvanced(true);
					}
	
					//if child.attrib['type'] == PARAM_SELECT():
					// TODO: implement select case
					//    input_parameter.selectInfo = BiCmdSelect()
	
					String defaultValue = child.getAttribute("default");
					input_parameter.setDefaultValue(defaultValue);
					input_parameter.setValue(defaultValue);
	
					input_parameter.setValueSuffix("");
	
					this.info.addInput(input_parameter);
				}
				else if ( child.getNodeName().equals("data") ) {    
					BiProcessParameter input_parameter = new BiProcessParameter();
					input_parameter.setIo(BiTypes.IO_INPUT());
					input_parameter.setData(true);
	
					input_parameter.setName(child.getAttribute("name"));
					input_parameter.setDescription(child.getAttribute("label"));
	
					String defaultValue = child.getAttribute("default");
					input_parameter.setDefaultValue(defaultValue);
					input_parameter.setValue(defaultValue);
	
					String format = child.getAttribute("format");
					if ( format.equals("image")) {
						input_parameter.setType(BiTypes.DATA_IMAGE());
					}
					else if ( format.equals("txt")) {
						input_parameter.setType(BiTypes.DATA_TXT());
					}
					else if ( format.equals("array")) {
						input_parameter.setType(BiTypes.DATA_ARRAY());
					}
					else if ( format.equals("matrix")) {
						input_parameter.setType(BiTypes.DATA_MATRIX());
					}
					else {
						System.out.println("The format of the input data " + input_parameter.getName() + " is not supported");
					}
	
					input_parameter.setValueSuffix(this.parseParametervalueSuffix(input_parameter.getName()));
	
					this.info.addInput(input_parameter);
				}
			}
		}
	}

	// Parse the command line to search if a input data have a suffix (ex: $(data)_suffix)"""
	private String parseParametervalueSuffix(String parameter_name) {

		String suffix = "";
		String search = "${"+parameter_name+"}";
		Vector<String> args = this.info.getCommandArgs();
		for (int i = 0 ; i < args.size() ; i++) {
			String arg = args.elementAt(i);
			if (arg.startsWith(search)) {
				suffix = arg.replace(search, "");
				return suffix;
			}
		}
		return suffix;

	}
	// Parse the outputs."""
	private void parseOutputs(Element node) {  

		final NodeList childs = node.getChildNodes();
		for (int i = 0; i<childs.getLength(); i++) 
		{
			if ( !childs.item(i).getNodeName().equals("#text")) {
				final Element child = (Element) childs.item(i);
	
				if ( child.getNodeName() == "data") {
	
					BiProcessParameter output_parameter = new BiProcessParameter();
					output_parameter.setIo(BiTypes.IO_OUTPUT());
					output_parameter.setData(true);
	
	
					output_parameter.setName(child.getAttribute("name"));
					output_parameter.setDescription(child.getAttribute("label"));
	
					String defaultvalue = child.getAttribute("default");
					output_parameter.setDefaultValue(defaultvalue);
					output_parameter.setValue(defaultvalue);
	
					String format = child.getAttribute("format");
					if ( format.equals("image") ) {
						output_parameter.setType(BiTypes.DATA_IMAGE());
					}
					else if ( format.equals("txt") ) {
						output_parameter.setType(BiTypes.DATA_TXT());
					}
					else if ( format.equals("number") ) {
						output_parameter.setType(BiTypes.DATA_NUMBER());
					}
					else if ( format.equals("array") ) {
						output_parameter.setType(BiTypes.DATA_ARRAY());
					}
					else if ( format.equals("matrix") ) {
						output_parameter.setType(BiTypes.DATA_MATRIX());
					}
					else if ( format.equals("table") ) {
						output_parameter.setType(BiTypes.DATA_TABLE());
					}
					else {
						System.out.println("The format of the output data " + output_parameter.name + " is not supported");
					}
	
					output_parameter.setValueSuffix(this.parseParametervalueSuffix(output_parameter.getName()));
					this.info.addOutput(output_parameter); 
				}
			}
		}
	}
	
	/*
	 *  Parse the hidden parameters.
        Hidden parameters state for the parameters in the command line that are 
        not accessible by the user

	 */
	private void parseHiddenParams() {


        // Parse the program name
        if (this.info.getCommandArgsSize() > 0) {
        	this.info.setProgram(this.info.getCommandArgAt(0));
        }
        else {
            System.out.println("Cannot parse the command line");
            return;
        }

        // Parse hidden parameters
        for (int i = 0 ; i < this.info.getCommandArgsSize() ; i++) {
        	String arg = this.info.getCommandArgAt(i);
        	if ( !arg.startsWith("${") && !this.info.isParam(arg) && !arg.equals(this.info.getProgram()) ) {
        		BiProcessParameter param = new BiProcessParameter();
        		param.setName(arg);
        		param.setType(BiTypes.PARAM_HIDDEN());
        		this.info.addInput(param);
        	}
        }

	}
}
