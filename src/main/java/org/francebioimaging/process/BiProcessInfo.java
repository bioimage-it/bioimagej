package org.francebioimaging.process;

import java.util.Vector;

import org.francebioimaging.core.BiTypes;

public class BiProcessInfo {

	String xmlFileUrl;
	String id;
	String name;
	String version;
	String description;
	String command;
	Vector<String> commandArgs;
	String program;
	Vector<BiProcessParameter> inputs;
	Vector<BiProcessParameter> outputs;
	String help;
	Vector<String> categories;
	String type;
	
	public BiProcessInfo() {
		xmlFileUrl = "";
		id = "";
		name = "";
		version = "";
		description = "";
		command = "";
		commandArgs = new Vector<String>();
		program = "";
		inputs = new Vector<BiProcessParameter>();
		outputs = new Vector<BiProcessParameter>();
		help = "";
		categories = new Vector<String>();
		type = "sequential";
	}
	
    public boolean isParam(String name) {

    	for(int i = 0 ; i < inputs.size() ; i++) {
    		if (inputs.elementAt(i).getName().equals(name)) {
    			return true;
    		}
    	} 
    	
    	for(int i = 0 ; i < outputs.size() ; i++) {
    		if (outputs.elementAt(i).getName().equals(name)) {
    			return true;
    		}
    	}

        return false;   
    }

    public int paramSize() {
        int count = 0;
        for(int i = 0 ; i < inputs.size() ; i++) {
        	if (inputs.elementAt(i).getIo() == BiTypes.IO_PARAM()) {
        		count++;
        	}
        }
        return count;        		
    }
    
    public void display() {

    	System.out.println("BiProcessInfo");
    	System.out.println("-------------");
    	System.out.println("id:" + id);  
    	System.out.println("name:" + name);
    	System.out.println("version:" + version); 
    	System.out.println("description:" + description); 
    	System.out.println("help:" + help); 
    	System.out.println("program:" + program); 
    	System.out.println("command:" + command); 
    	System.out.println("args:" + command); 
    	System.out.println("inputs:");
    	for (int i = 0 ; i < inputs.size() ; i++) {
    		inputs.elementAt(i).display();
    	}
    	for (int i = 0 ; i < outputs.size() ; i++) {
    		outputs.elementAt(i).display();
    	}

    }
    
    public Vector<BiProcessParameter> getInputData(){
    	Vector<BiProcessParameter> inputData = new Vector<BiProcessParameter>();
    	for (int i = 0 ; i < inputs.size() ; i++) {
    		if ( inputs.get(i).isData() ) {
    			inputData.add(inputs.get(i));
    		}
    	}
		return inputData;
    	
    }

    public int inputsSize() {
    	return inputs.size();
    }

    public int outputsSize() {
    	return outputs.size();
    }

	public String getXmlFileUrl() {
		return xmlFileUrl;
	}

	public void setXmlFileUrl(String xmlFileUrl) {
		this.xmlFileUrl = xmlFileUrl;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Vector<String> getCommandArgs() {
		return commandArgs;
	}
	
	public int getCommandArgsSize() {
		return commandArgs.size();
	}
	
	public String getCommandArgAt(int index) {
		return commandArgs.elementAt(index);
	}

	public void setCommandArgs(Vector<String> commandArgs) {
		this.commandArgs = commandArgs;
	}

	public String getProgram() {
		return program;
	}

	public void setProgram(String program) {
		this.program = program;
	}

	public Vector<BiProcessParameter> getInputs() {
		return inputs;
	}

	public void setInputs(Vector<BiProcessParameter> inputs) {
		this.inputs = inputs;
	}
	
	public void addInput(BiProcessParameter input) {
		this.inputs.add(input);
	}
	
	public BiProcessParameter getInputAt(int index) {
		return this.inputs.get(index);
	}

	public Vector<BiProcessParameter> getOutputs() {
		return outputs;
	}

	public void setOutputs(Vector<BiProcessParameter> outputs) {
		this.outputs = outputs;
	}
	
	public void addOutput(BiProcessParameter output) {
		this.outputs.add(output);
	}

	public String getHelp() {
		return help;
	}

	public void setHelp(String help) {
		this.help = help;
	}

	public Vector<String> getCategories() {
		return categories;
	}

	public void setCategories(Vector<String> categories) {
		this.categories = categories;
	}
	
	public void addCategory(String category) {
		this.categories.add(category);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getArgsCmd() {
		String argsCmd = "";
		for (int p = 0 ; p < inputs.size() ; p++) {
			if ( ! inputs.get(p).isData() && inputs.get(p).getType() != BiTypes.PARAM_HIDDEN() ) {
				argsCmd += inputs.get(p).getName() + "=" + inputs.get(p).getValue()+ " ";
			}
		}
		argsCmd = argsCmd.substring(0, argsCmd.length() - 1);
		return argsCmd;
	}

	public void setArgs(String args) {
		String[] argsArray = args.split(" ");
		for (int i = 0; i < argsArray.length; i++) {
			String[] argPair = argsArray[i].split("=");
			if (argPair.length == 2) {
				for ( int p = 0 ; p < inputs.size() ; p++) {
					if ( inputs.get(p).getName().equals(argPair[0]) ) {
						inputs.get(p).setValue(argPair[1]);
					}
				}
			}
			else {
				System.out.println("The arguments " + argsArray[i] + " are not of type key=value");
			}
		}
	}
}
