package org.francebioimaging.core;

public class BiTypes {

	public static String Type() {
		return "";
	}

	public static String DATA_IMAGE() {
		//Type for data image""" 

		return "image";
	}

	public static String DATA_TXT() {
		//Type for data text""" 

		return "txt";   
	}

	public static String DATA_NUMBER() {
		//Type for data number"""

		return "number";
	}

	public static String DATA_ARRAY() {
		//Type for data array"""

		return "array";  
	}

	public static String DATA_MATRIX() {
		//Type for data matrix"""

		return "matrix";   
	}

	public static String DATA_TABLE() {
		//Type for data table"""

		return "table";
	}

	public static String PARAM_NUMBER() {
		//Type for parameter number""" 

		return "number";
	}

	public static String PARAM_STRING() {
		//Type for parameter string""" 

		return "string";
	}

	public static String PARAM_SELECT() {
		//Type for parameter select""" 

		return "select";
	}

	public static String PARAM_BOOLEAN() {
		//Type for parameter boolean""" 

		return "boolean";
	}

	public static String PARAM_HIDDEN() {
		//Type for parameter hidden""" 

		return "hidden"; 
	}

	public static String PARAM_FILE() {
		//Type for parameter hidden""" 

		return "file";  
	}

	public static String IO_PARAM() {
		//I/O for parameter""" 

		return "param";
	}

	public static String IO_INPUT() {
		//I/O for data input"""

		return "input"; 
	}

	public static String IO_OUTPUT() {
		//I/O for data output"""

		return "output";   
	}
}
