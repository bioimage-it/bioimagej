package org.francebioimaging.core;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.border.AbstractBorder;
import javax.swing.border.EmptyBorder;

public class BiStylesheet {

	public static void defaultBackground(JPanel panel) {
		panel.setBackground(new Color(51,51,51));
	}
	
	public static void defaultToolBarBackground(JPanel panel) {
		panel.setBackground(new Color(51,51,51));
		panel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(100,100,100)));
	}

	public static void sectionTitle(JLabel label) {
		label.setFont(new Font("Serif", Font.BOLD, 16));
		label.setForeground(new Color(200,200,200));
		label.setHorizontalAlignment( SwingConstants.LEFT );
		label.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, new Color(200,200,200)));
	}

	public static void defaultToolbar(JToolBar toolbar) {
		toolbar.setBackground(new Color(51,51,51));
	}
	
	public static void defaultTable(JTable table) {
		table.setForeground(new Color(200,200,200));

        table.setOpaque(true);
        table.setFillsViewportHeight(true);
        table.setBackground(new Color(51,51,51));
		
		table.getTableHeader().setBackground(new Color(51,51,51));
		table.getTableHeader().setForeground(new Color(200,200,200));
	}
	
	
	public static void defaultLabel(JLabel label) {
		label.setForeground(new Color(200,200,200));
	}

	public static void pluginTitle(JLabel label) {
		label.setFont(new Font("Serif", Font.BOLD, 16));
		label.setForeground(new Color(230,230,230));
		label.setHorizontalAlignment( SwingConstants.CENTER );
	}

	public static void defaultTextField(JTextField textField) {
		Font fieldFont = new Font("Arial", Font.PLAIN, 14);
		textField.setFont(fieldFont);
		textField.setBackground(Color.white);
		textField.setForeground(new Color(51,51,51));
		textField.setColumns(30);
		textField.setBorder(BorderFactory.createCompoundBorder(
				new CustomeBorder(), 
				new EmptyBorder(new Insets(15, 25, 15, 25))));
	}

	public static void defaultButton(JButton button) {
		Font fieldFont = new Font("Arial", Font.PLAIN, 14);
		button.setFont(fieldFont);
		button.setBackground(Color.white);
		button.setForeground(new Color(51,51,51));
	}
	
	public static void browserTile(JPanel panel) {
		panel.setBackground(new Color(90,142,142));
	}
	
}

@SuppressWarnings("serial")
class CustomeBorder extends AbstractBorder{
    @Override
    public void paintBorder(Component c, Graphics g, int x, int y,
            int width, int height) {
        super.paintBorder(c, g, x, y, width, height);
        Graphics2D g2d = (Graphics2D)g;
        g2d.setStroke(new BasicStroke(12));
        g2d.setColor(new Color(51,51,51));
        g2d.drawRoundRect(x, y, width - 1, height - 1, 3, 3);
    }   
}
