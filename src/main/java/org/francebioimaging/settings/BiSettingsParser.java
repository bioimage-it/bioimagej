package org.francebioimaging.settings;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class BiSettingsParser {

	protected String settingsFile;


	public BiSettingsParser(String file) {
		settingsFile = file;
	}

	public BiSettingsSet parse() {
		BiSettingsSet settingsSet = new BiSettingsSet();
		settingsSet.setJsonFile(settingsFile);
		
		String jsonString = "";
		try {
			jsonString = BiSettingsParser.readFile(settingsFile, StandardCharsets.US_ASCII);
		} catch (IOException e) {
			e.printStackTrace();
		}

		JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();
		List<String> keys = jsonObject.entrySet()
				.stream()
				.map(i -> i.getKey())
				.collect(Collectors.toCollection(ArrayList::new));

		for (int g = 0 ; g < keys.size() ; g++) {
			BiSettingsGroup group = new BiSettingsGroup();
			group.setName(keys.get(g));
			JsonArray arr = jsonObject.getAsJsonArray(keys.get(g));
			for (int i = 0; i < arr.size(); i++) {
				JsonObject jsonObject_i = arr.get(i).getAsJsonObject();
				Gson gson = new Gson(); 
				BiSettingInfo setting = gson.fromJson(jsonObject_i, BiSettingInfo.class);
				group.addSetting(setting);
			}
			settingsSet.addGroup(group);

		}

		return settingsSet;

	}

	public void save(BiSettingsSet settingsSet) {


		JsonObject jsonObject = new JsonObject();


		for (int g = 0 ; g < settingsSet.size() ; g++) {

			BiSettingsGroup group = settingsSet.group(g);

			JsonArray jsonArray = new JsonArray();
			for (int i = 0 ; i < group.size() ; i++) {
				Gson gson = new Gson();
				jsonArray.add(gson.toJsonTree(group.get(i)));
			}

			jsonObject.add(group.getName(), jsonArray);
		}

		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(settingsFile));
			writer.write(toPrettyFormat(jsonObject.toString()));
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}


	}

	public static String toPrettyFormat(String jsonString) 
	{
		JsonParser parser = new JsonParser();
		JsonObject json = parser.parse(jsonString).getAsJsonObject();

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String prettyJson = gson.toJson(json);

		return prettyJson;
	}

	public static String readFile(String path, Charset encoding) 
			throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
}
