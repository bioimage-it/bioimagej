package org.francebioimaging.settings;

import java.util.Vector;

public class BiSettingsSet {

	protected Vector<BiSettingsGroup> settingsGroups;
	protected String jsonFile;
	
	public String getJsonFile() {
		return jsonFile;
	}

	public void setJsonFile(String jsonFile) {
		this.jsonFile = jsonFile;
	}

	public BiSettingsSet() {
		settingsGroups = new Vector<BiSettingsGroup>();
	}
	
	public void print() {
		for (int i = 0 ; i < settingsGroups.size() ; i++) {
			settingsGroups.get(i).print();
		}
	}
	
	public String getValue(String group, String key) {
		for (int i = 0 ; i < settingsGroups.size() ; i++) {
			if ( settingsGroups.get(i).getName().equals(group) ) {
				BiSettingsGroup sgroup = settingsGroups.get(i);
				for (int j = 0 ; j < sgroup.size() ; j++) {
					if ( sgroup.getSettingInfo(j).getKey().equals(key) ) {
						return sgroup.getSettingInfo(j).getValue();
					}
				}
			}
		}
		return "";
	}
	
	public int size() {
		return settingsGroups.size();
	}
	
	public BiSettingsGroup group(int i ) {
		return settingsGroups.get(i);
	}
	
	public BiSettingsGroup get(int i ) {
		return settingsGroups.get(i);
	}
	
	public void addGroup(BiSettingsGroup group) {
		settingsGroups.add(group);
	}
	
	
}
