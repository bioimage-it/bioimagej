package org.francebioimaging.settings;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.francebioimaging.core.BiStylesheet;

public class BiSettingEditor extends JPanel implements ActionListener {

	
	private static final long serialVersionUID = 1L;
	protected JTextField valueEdit;
	protected JButton browseButton;
	protected BiSettingInfo info;

	public BiSettingEditor(BiSettingInfo info) {
		super();
		
		this.info = info;
		BoxLayout layout = new BoxLayout(this, BoxLayout.X_AXIS);
		this.setLayout(layout);

		this.valueEdit = new JTextField();
		BiStylesheet.defaultBackground(this);
		BiStylesheet.defaultTextField(this.valueEdit);
		this.valueEdit.setText(info.getValue());
		valueEdit.setMaximumSize( valueEdit.getPreferredSize() );
		this.add(this.valueEdit);
		
		if ( info.getType().equals(BiSettingsTypes.DIR) || info.getType().equals(BiSettingsTypes.FILE) ){
			browseButton = new JButton("...");
			browseButton.addActionListener(this);
			this.add(browseButton);
		}

		this.valueEdit.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void insertUpdate(DocumentEvent e) {
				warn();
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				warn();
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				warn();
			}

			public void warn() {
				info.setValue(valueEdit.getText());
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		JFileChooser chooser = new JFileChooser();
		if (info.getType().equals(BiSettingsTypes.DIR)) {
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			chooser.setAcceptAllFileFilterUsed(false);
		}
		int rVal = chooser.showOpenDialog(this);
		if (rVal == JFileChooser.APPROVE_OPTION) {
			try {
				this.valueEdit.setText(chooser.getSelectedFile().getCanonicalPath());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return;
		
	}
}
