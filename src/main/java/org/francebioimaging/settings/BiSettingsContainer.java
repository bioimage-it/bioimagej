package org.francebioimaging.settings;

import org.francebioimaging.framework.BiContainer;

public class BiSettingsContainer extends BiContainer {

	public static String SettingsFileChanged = "BiSettingsContainer.SettingsFileChanged";
	public static String SettingsLoaded = "BiSettingsContainer.SettingsLoaded";
	public static String SettingsChanged = "BiSettingsContainer.SettingsChanged";
	public static String SettingsSaved = "BiSettingsContainer.SettingsSaved";
	
	
	protected String settingsFile;
	protected BiSettingsSet settingsSet;
	
	public BiSettingsContainer() {
		settingsFile = "";
	}
	
	public String getSettingsFile() {
		return settingsFile;
	}
	
	public void setSettingsFile(String settingsFile) {
		this.settingsFile = settingsFile;
	}
	
	public void setSettingsSet(BiSettingsSet settingsSet){
		this.settingsSet = settingsSet;
	}
	
	public BiSettingsSet getSettingsSet(){
		return settingsSet;
	}
	
}
