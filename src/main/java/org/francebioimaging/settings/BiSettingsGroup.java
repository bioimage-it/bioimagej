package org.francebioimaging.settings;

import java.util.Vector;

public class BiSettingsGroup {

	protected String name;
	protected Vector<BiSettingInfo> settingsInfo;
	
	public BiSettingsGroup() {
		name = "";
		settingsInfo = new Vector<BiSettingInfo>();
	}
	
	public void print() {
		System.out.println("group: " + name);
		for (int i = 0 ; i < settingsInfo.size() ; i++) {
			settingsInfo.get(i).print();
		}
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public int size() {
		return settingsInfo.size();
	}
	
	public void addSetting(BiSettingInfo info) {
		settingsInfo.add(info);
	}
	
	public BiSettingInfo getSettingInfo(int i) {
		return settingsInfo.get(i);
	}
	
	public BiSettingInfo get(int i) {
		return settingsInfo.get(i);
	}
}
