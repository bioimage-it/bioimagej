package org.francebioimaging.settings;

import org.francebioimaging.framework.BiContainer;
import org.francebioimaging.framework.BiModel;

public class BiSettingsModel extends BiModel {
	
	BiSettingsContainer container;
	
	public BiSettingsModel(BiSettingsContainer container) {
		super();
		this.container = container;
		this.container.addObserver(this);
	}
	
	public void update(BiContainer container, String action) {
		
		if ( action.equals( BiSettingsContainer.SettingsFileChanged )) {
			BiSettingsParser parser = new BiSettingsParser(this.container.getSettingsFile());
			BiSettingsSet settingsSet = parser.parse();
			
			this.container.setSettingsSet(settingsSet);
			this.container.notify(BiSettingsContainer.SettingsLoaded);
		}
		
		if ( action.equals( BiSettingsContainer.SettingsChanged )) {
			BiSettingsParser parser = new BiSettingsParser(this.container.getSettingsFile());
			parser.save(this.container.getSettingsSet());
			
			this.container.notify(BiSettingsContainer.SettingsSaved);
		}
		
	}

}
