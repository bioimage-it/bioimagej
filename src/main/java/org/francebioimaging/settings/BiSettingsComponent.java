package org.francebioimaging.settings;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import org.francebioimaging.core.BiStylesheet;
import org.francebioimaging.framework.BiComponent;
import org.francebioimaging.framework.BiContainer;

public class BiSettingsComponent extends BiComponent implements ActionListener {

	protected BiSettingsContainer container;
	protected JPanel scrollWidget;
	protected GridBagConstraints gbc;
	
	public BiSettingsComponent(BiSettingsContainer container) {
		this.container = container;
		this.container.addObserver(this);
		
		this.widget = new JPanel();
        BoxLayout centralWidgetLayout = new BoxLayout(this.widget, BoxLayout.Y_AXIS);
        this.widget.setLayout(centralWidgetLayout);
		BiStylesheet.defaultBackground(this.widget);
		
		int v = ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED ;
        int h = ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED ;
        
        scrollWidget = new JPanel();
        BiStylesheet.defaultBackground(scrollWidget);
       
		GridBagLayout layoutScroll = new GridBagLayout();
		scrollWidget.setLayout(layoutScroll);
        gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.weighty = 5;
        //gbc.fill = GridBagConstraints.VERTICAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        
        
        JScrollPane scrollArea = new JScrollPane( scrollWidget, v, h ) ;
        
		scrollArea.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.widget.add(scrollArea) ;
	}
	
	public void buildWidget() {
		
		for (int g = 0 ; g < this.container.getSettingsSet().size() ; g++) {
			BiSettingsGroup group = this.container.getSettingsSet().group(g);
			BiGroupEditorWidget groupEditor = new BiGroupEditorWidget(group);
			scrollWidget.add(groupEditor, gbc);
		}
		
		JButton saveButton = new JButton("Save");
		BiStylesheet.defaultButton(saveButton);
		scrollWidget.add(saveButton, gbc);
		saveButton.addActionListener(this);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		this.container.notify(BiSettingsContainer.SettingsChanged);
	}
	
	public void update(BiContainer container, String action) {
		
		if (action.equals(BiSettingsContainer.SettingsLoaded)) {
			scrollWidget.removeAll();
			buildWidget();
			scrollWidget.revalidate();
			scrollWidget.repaint();
			this.widget.revalidate();
			this.widget.repaint();
			return;
		}
		
		if (action.equals(BiSettingsContainer.SettingsSaved)) {
			JOptionPane.showMessageDialog(this.widget, "Settings have been saved.");
			return;
		}
		
	}
}
