package org.francebioimaging.settings;

import java.util.Vector;

public class BiSettingInfo {

    protected String key;
    protected String value;
    protected String type;
    protected Vector<String> choices; // for select type
    
	public BiSettingInfo() {
		key = "";
		value = "";
		type = "";
		choices = new Vector<String>();
	}
	
	public void print() {
		System.out.println("-------");
		System.out.println("key:" + key);
		System.out.println("value:" + value);
		System.out.println("type:" + type);
		System.out.println("");
	}
	
	public String getKey() {
		return key;
	}


	public void setKey(String key) {
		this.key = key;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String getType() {
		return type;
	}

	
	public void setType(String type) {
		this.type = type;
	}
	
	
	public void addChoice(String choice) {
		choices.add(choice);
	}
	
	
	public int choicesSize() {
		return choices.size();
	}
	
	
	public String getChoice(int i) {
		return choices.get(i);
	}
	

}
