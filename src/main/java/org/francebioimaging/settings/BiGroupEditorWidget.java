package org.francebioimaging.settings;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.francebioimaging.core.BiStylesheet;

public class BiGroupEditorWidget extends JPanel{

	private static final long serialVersionUID = 1L;
	protected BiSettingsGroup group;
	
	public BiGroupEditorWidget(BiSettingsGroup group) {
		
		BiStylesheet.defaultBackground(this);
		GridLayout experimentLayout = new GridLayout(0,2);
		this.setLayout(experimentLayout);
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		JLabel label = new JLabel(group.getName());
		BiStylesheet.sectionTitle(label);
		this.add(label);
		JLabel fillLabel = new JLabel();
		BiStylesheet.sectionTitle(fillLabel);
		this.add(fillLabel);
		
		for (int i = 0 ; i < group.size() ; i++) {
			BiSettingEditor editor = new BiSettingEditor(group.get(i));
			
			JLabel titleLabel = new JLabel(group.get(i).getKey());
			BiStylesheet.defaultLabel(titleLabel);
			this.add(titleLabel);
			this.add(editor);
		}
		
	}
	
}
