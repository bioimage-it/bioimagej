package org.francebioimaging.settings;

public class BiSettingsTypes {
	
	public static String STRING = "string";
	public static String NUMBER = "number";
	public static String SELECT = "select";
	public static String FILE = "file";
	public static String DIR = "dir";
}
