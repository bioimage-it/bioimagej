package org.francebioimaging.browser;

import org.francebioimaging.framework.BiContainer;
import org.francebioimaging.framework.BiModel;

import org.francebioimaging.process.BiProcessParser;

import java.nio.file.Paths;
import java.io.File;
import java.io.IOException;

public class BiBrowserModel extends BiModel {

	protected BiBrowserContainer container;

	public BiBrowserModel(BiBrowserContainer container) {
		super();
		this.objectName = "BiProcessesBrowserContainer";
		this.container = container;
		this.container.addObserver(this);
	}

	public void update(BiContainer container, String action) {
		if (action.equals(BiBrowserContainer.ProcessesDirChanged)){
			if (this.load()){
				this.container.notify(BiBrowserContainer.ProcessesLoaded);
			}
		}
	}

	public boolean load() {
		if ( !this.loadProcesses() ) {
			return false;
		}
		if ( !this.loadCategories() ) {
			return false; 
		}
		return true;  
	}

	public boolean loadCategories() {
		String categories_file = Paths.get(this.container.getCategoriesJsonFile()).toString();
		File f = new File(categories_file);
		if(f.exists() && !f.isDirectory()) { 
			BiCategoryLoader loader = new BiCategoryLoader();
			this.container.setCategories(loader.load(categories_file));
			return true;
		}
		return false;
	}

	public boolean loadProcesses(String directory) {
		File processesDirInfo = new File(directory);

		if (processesDirInfo.exists() && processesDirInfo.isDirectory()) {
			File[] listOfFiles = processesDirInfo.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
				try {
					String xmlPath = listOfFiles[i].getCanonicalPath();
					File currentFile = new File(xmlPath);
					if ( currentFile.isDirectory() ) {
						loadProcesses(xmlPath);
					}
					else if (xmlPath.endsWith(".xml")) {
						BiProcessParser parser = new BiProcessParser(xmlPath);
						this.container.addProcess(parser.parse());
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return true;
		}
		else
		{
			System.out.println("WARNING: biProcessesModel::load: the processed dir " + this.container.getProcessesDir()+ " does not exists");
			return false;
		}
	}
	
	public boolean loadProcesses() {

		return loadProcesses(this.container.getProcessesDir());
	}
}
