package org.francebioimaging.browser;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Vector;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class BiCategoryLoader {

	public Vector<BiCategory> load(String file){

		Vector<BiCategory> categories = new Vector<BiCategory>();
		
		String jsonString = "";
		try {
			jsonString = BiCategoryLoader.readFile(file, StandardCharsets.US_ASCII);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();
		
		JsonArray arr = jsonObject.getAsJsonArray("categories");
        for (int i = 0; i < arr.size(); i++) {
        	JsonObject jsonObject_i = arr.get(i).getAsJsonObject();
        	Gson gson = new Gson(); 
        	BiCategory category = gson.fromJson(jsonObject_i, BiCategory.class);
        	categories.add(category);
        }
		return categories;
	}
	
	public static String readFile(String path, Charset encoding) 
			  throws IOException 
	{
	  byte[] encoded = Files.readAllBytes(Paths.get(path));
	  return new String(encoded, encoding);
	}
}
