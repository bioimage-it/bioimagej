package org.francebioimaging.browser;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.francebioimaging.process.BiProcessInfo;

public class BiProcessInfoCellRenderer extends AbstractCellEditor implements TableCellEditor, TableCellRenderer,  ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BiBrowserContainer container;
	BiProcessInfo info;
	
	public BiProcessInfoCellRenderer(BiBrowserContainer container) {
		this.container = container;
	}
	
	
    @Override
    public Object getCellEditorValue() {
        return info;
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		container.setClickedProcess(info);
		container.notify(BiBrowserContainer.OpenProcess);
		System.out.println("open process clicke " + info.getName());
		
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		
		info = (BiProcessInfo)value;
		JButton button = new JButton("Open");
		button.addActionListener(this);
		
		return button;
	}


	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		
		info = (BiProcessInfo)value;
		JButton button = new JButton("Open");
		button.addActionListener(this);
		
		return button;
	}

}
