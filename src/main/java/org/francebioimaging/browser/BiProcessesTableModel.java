package org.francebioimaging.browser;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import org.francebioimaging.process.BiProcessInfo;

public class BiProcessesTableModel extends AbstractTableModel {

	
	private static final long serialVersionUID = 1L;

	private Vector<BiProcessInfo> processes;
 
    private final String[] headers = {"Open", "Name", "Description", "Version"};
 
    public BiProcessesTableModel(Vector<BiProcessInfo> processes) {
        super();
 
        this.processes = processes;
        
    }
 
    public int getRowCount() {
        return processes.size();
    }
 
    public int getColumnCount() {
        return 4;
    }
 
    public String getColumnName(int columnIndex) {
        return headers[columnIndex];
    }
 
    public Object getValueAt(int rowIndex, int columnIndex) {
    	if ( columnIndex == 0 ) {
    		return processes.get(rowIndex);
    	}
    	else if ( columnIndex == 1 ) {
    		return processes.get(rowIndex).getName();
    	}
    	else if ( columnIndex == 2 ) {
    		return processes.get(rowIndex).getDescription();
    	}
    	else if ( columnIndex == 3 ) {
    		return processes.get(rowIndex).getVersion();
    	}
    	else {
    		return null;
    	}
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }
    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
    public Class getColumnClass(int columnIndex){
    	switch(columnIndex){
    		case 0:
    			return BiProcessInfo.class;
    		default:
    			return String.class;
    	}
    }
}