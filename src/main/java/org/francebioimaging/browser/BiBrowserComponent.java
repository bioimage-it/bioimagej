package org.francebioimaging.browser;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import org.francebioimaging.core.BiStylesheet;
import org.francebioimaging.externals.WrapLayout;
import org.francebioimaging.framework.BiComponent;
import org.francebioimaging.framework.BiContainer;
import org.francebioimaging.process.BiProcessInfo;

public class BiBrowserComponent extends BiComponent {
	
	protected BiBrowserContainer container;
	protected Vector<String> historyPaths;
	protected int posHistory;
	protected String currentPath;
	protected BiNavigationBarComponent navBar;
	protected JScrollPane browseWidget;
	protected JPanel scrollWidget;
	protected JPanel tablePanel;
	protected JTable tableWidget;
	protected JPanel centralWidget;
	
    public BiBrowserComponent(BiBrowserContainer container) {
        super();
        this.objectName = "BiProcessesComponent";
        this.container = container;
        this.container.addObserver(this); 
        this.historyPaths = new Vector<String>();
        this.posHistory = 0;
        this.currentPath = "";

        // Widget
        this.widget = new JPanel();
        BiStylesheet.defaultBackground(this.widget);
        
        BoxLayout layout = new BoxLayout(this.widget, BoxLayout.Y_AXIS);
        //BorderLayout layout = new BorderLayout();
        this.widget .setLayout(layout);
        
        // NavBar
        BiNavigationBarContainer navigationBarContainer = new BiNavigationBarContainer();
        navigationBarContainer.addObserver(this);
        this.navBar = new BiNavigationBarComponent(navigationBarContainer);
        //this.widget.add(this.navBar.getWidget(), BorderLayout.PAGE_START);
        this.widget.add(this.navBar.getWidget());

        
        // central widget
        centralWidget = new JPanel();
        BoxLayout centralWidgetLayout = new BoxLayout(centralWidget, BoxLayout.Y_AXIS);
        centralWidget.setLayout(centralWidgetLayout);
        BiStylesheet.defaultBackground(centralWidget);

        // Browse area
        scrollWidget = new JPanel();
        BiStylesheet.defaultBackground(scrollWidget);
        WrapLayout layoutScroll = new WrapLayout();
        scrollWidget.setLayout(layoutScroll);
        
        
        int v = ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED ;
        int h = ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED ;
     
        browseWidget = new JScrollPane( scrollWidget, v, h ) ;
        browseWidget.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.widget.add(browseWidget) ;
        centralWidget.add(browseWidget);
        

        // tools table
        tablePanel = new JPanel();
        BiStylesheet.defaultBackground(tablePanel);
        this.widget.add(tablePanel);
        tablePanel.setVisible(false);
        centralWidget.add(tablePanel);
        
    }
    
    public JPanel getToolBar() {
    	return this.navBar.getWidget();
    }
    
    public JPanel getCentralWidget() {
    	return centralWidget;
    }
    
    public void update(BiContainer container, String action) {
    	if ( action.equals( BiNavigationBarContainer.PreviousClicked )) {
    		this.container.moveToPrevious();
    		this.container.notify(BiBrowserContainer.PathChanged);
    		return;
    	}
    	if ( action.equals( BiNavigationBarContainer.NextClicked )) {
    		this.container.moveToNext();
    		this.container.notify(BiBrowserContainer.PathChanged);
    		return;
    	}
    	if ( action.equals( BiNavigationBarContainer.HomeClicked )) {
    		this.container.moveToHome();
    		this.container.notify(BiBrowserContainer.PathChanged);
    		return;
    	}
        if ( action.equals( BiBrowserContainer.ProcessesLoaded ) 
        		|| action.equals( BiBrowserContainer.PathChanged ) ) {
                this.navBar.setPath(this.container.getCurrentPathName());
                
                this.browse(this.container.getCategories(),this.container.getProcessesDir(), this.container.getCurrentPath());
                return;  
        } 
    }

    public void browse(Vector<BiCategory> categories, String processesDir, String parent) {
    	if (this.hasChildCategory(parent)) {
    		this.browseCategories(categories, processesDir, parent);
    		this.tablePanel.setVisible(false);
    		this.browseWidget.setVisible(true);
    	}
    	else {
            this.browseTools(categories, processesDir, parent); 
            this.tablePanel.setVisible(true);
            this.browseWidget.setVisible(false);
    	}
        tablePanel.revalidate();
        tablePanel.repaint();
        browseWidget.revalidate();
        browseWidget.repaint();
    }
    
    public boolean hasChildCategory(String parent) {
    	for (int i = 0 ; i < this.container.categories.size() ; i++) {
    		if ( this.container.categories.get(i).getParent().equals(parent) ) {
    			return true;
    		}
    	}
    	return false;
    }
    
    public void browseTools(Vector<BiCategory> categories, String processesDir, String parent) {
    	
        Vector<BiProcessInfo> processes = new Vector<BiProcessInfo>();
    	for (int p = 0 ; p < this.container.getProcesses().size() ; p++) {
    		BiProcessInfo processInfo = this.container.getProcesses().get(p);
    		Vector<String> categoriesNames = processInfo.getCategories();
    		for ( int c = 0 ; c < categoriesNames.size() ; c++) {
    			if ( categoriesNames.get(c).equals( parent ) ) {
    				processes.add(processInfo);
    				break;
    			}
    		}
    		
    	}
    	
        tableWidget = new JTable(new BiProcessesTableModel(processes));
        tableWidget.setDefaultEditor(BiProcessInfo.class, new BiProcessInfoCellRenderer(this.container));
        tableWidget.setDefaultRenderer(BiProcessInfo.class, new BiProcessInfoCellRenderer(this.container));
        BiStylesheet.defaultTable(tableWidget);
       
        
        tablePanel.removeAll();
        tablePanel.setLayout(new BorderLayout());
        BiStylesheet.defaultBackground(tablePanel);
        tablePanel.add(tableWidget.getTableHeader(), BorderLayout.NORTH);
        
        JScrollPane scrollPane = new JScrollPane(tableWidget);
        scrollPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        tablePanel.add(scrollPane, BorderLayout.CENTER);
        
                
    }
    
    
    public void browseCategories(Vector<BiCategory> categories, String processesDir, String parent) {
    	
        // free layout
    	Component[] componentList = scrollWidget.getComponents();
    	for(Component c : componentList){
    		scrollWidget.remove(c);
    	}

        // browse
    	for ( int i = 0 ; i < categories.size() ; i++ ) {
    		BiCategory category = categories.get(i); 
    		if (category.getParent().equals(parent)) {
    			BiCategoryTileComponent widget = new BiCategoryTileComponent(this.container, category, processesDir);
    	        this.scrollWidget.add(widget.getWidget());
    		}
    	}	
    	
    	scrollWidget.validate();
    	scrollWidget.repaint();
    }
    
}


