package org.francebioimaging.browser;

import javax.swing.JToolBar;

import org.francebioimaging.core.BiStylesheet;
import org.francebioimaging.framework.BiComponent;
import org.francebioimaging.framework.BiContainer;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class BiNavigationBarComponent extends BiComponent {

	protected JToolBar toolBar;
	protected JTextField textField;
	
	public BiNavigationBarComponent( BiNavigationBarContainer container ) {
        super();
        
    
        // create toolBar
        widget = new JPanel();
        
        BoxLayout layout = new BoxLayout(widget, BoxLayout.X_AXIS);
        widget.setLayout(layout);
        BiStylesheet.defaultToolBarBackground(widget);
        
    	// previous
    	ImageIcon iconPrevious = new ImageIcon(this.getClass().getClassLoader().getResource("previous.png"));
    	Image imagePrevious = iconPrevious.getImage();
    	Image newimgPrevious = imagePrevious.getScaledInstance(32, 32,  java.awt.Image.SCALE_SMOOTH);  
    	iconPrevious = new ImageIcon(newimgPrevious); 
    	
    	JButton previousButton = new JButton(iconPrevious);
    	widget.add(previousButton);
    	previousButton.addActionListener((e) -> container.notify(BiNavigationBarContainer.PreviousClicked)); 

    	// next
    	ImageIcon iconNext = new ImageIcon(this.getClass().getClassLoader().getResource("next.png"));
    	Image image = iconNext.getImage();
    	Image newimg = image.getScaledInstance(32, 32,  java.awt.Image.SCALE_SMOOTH);  
    	iconNext = new ImageIcon(newimg); 
    	
    	JButton nextButton = new JButton(iconNext);
    	widget.add(nextButton);
    	nextButton.addActionListener((e) -> container.notify(BiNavigationBarContainer.NextClicked));
    	
    	// home
    	ImageIcon iconHome = new ImageIcon(this.getClass().getClassLoader().getResource("home.png"));
    	Image imageHome = iconHome.getImage();
    	Image newimgHome = imageHome.getScaledInstance(32, 32,  java.awt.Image.SCALE_SMOOTH);  
    	iconHome = new ImageIcon(newimgHome); 
    	
    	JButton homeButton = new JButton(iconHome);
    	widget.add(homeButton);
    	homeButton.addActionListener((e) -> container.notify(BiNavigationBarContainer.HomeClicked));
    	
    	// bar
    	textField = new JTextField();
		BiStylesheet.defaultTextField(textField);
		textField.addActionListener(new ActionListener() {
		      public void actionPerformed(ActionEvent e) {
		        System.out.println("Text=" + textField.getText());
		      }
		    });
		widget.add(textField);
		
    }	

    public void setPath(String path) {
    	textField.setText(path);
    }

	@Override
	public void update(BiContainer container, String action) {
		
	}

}
