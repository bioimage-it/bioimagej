package org.francebioimaging.browser;

import javax.swing.JPanel;

import org.francebioimaging.framework.BiComponent;
import org.francebioimaging.framework.BiContainer;

public class BiBrowserToolBarComponent extends BiComponent{
	
	protected BiBrowserContainer container;
	
    public BiBrowserToolBarComponent(BiBrowserContainer container) {
        super();
        this.objectName = "BiProcessesBrowserToolBarComponent";
        this.container = container;
        this.container.addObserver(this);  

        this.widget = new JPanel();
    }

    public void update(BiContainer container, String action) {
        
    }

}
