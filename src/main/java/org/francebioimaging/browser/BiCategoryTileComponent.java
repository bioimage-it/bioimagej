package org.francebioimaging.browser;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.nio.file.Paths;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.francebioimaging.core.BiStylesheet;
import org.francebioimaging.framework.BiComponent;
import org.francebioimaging.framework.BiContainer;

public class BiCategoryTileComponent extends BiComponent{

	protected BiCategory info;
	protected BiBrowserContainer container;

	public BiCategoryTileComponent(BiBrowserContainer container, BiCategory info, String processesDir) {
		super();
		this.info = info;
		this.container = container;

		this.widget = new JPanel();
		BiStylesheet.browserTile(this.widget);
		

		GridBagLayout layout = new GridBagLayout();
		this.widget.setLayout(layout);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.weightx = 1;
		gbc.weighty = 5;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.insets = new Insets(7,7,0,7);

		JLabel titleLabel = new JLabel(info.getName());
		this.widget.add(titleLabel, gbc);
		BiStylesheet.pluginTitle(titleLabel);

		JLabel thumbnailLabel = new JLabel();
		
		// get the categories directory
		File categoriesFile = new File(container.getCategoriesJsonFile());
		String categoriesDir = categoriesFile.getParent();
		
    	ImageIcon iconLogo = new ImageIcon(Paths.get(categoriesDir, info.getThumbnail()).toString());
    	Image imageLogo = iconLogo.getImage();
    	Image newimg = imageLogo.getScaledInstance(300, 150,  java.awt.Image.SCALE_SMOOTH);  
    	iconLogo = new ImageIcon(newimg); 
		
		thumbnailLabel.setIcon(iconLogo);
		this.widget.add(thumbnailLabel, gbc);

		JButton openButton = new JButton("Open");
		openButton.addActionListener(new OpenActionListener(container));
		this.widget.add(openButton, gbc);

	}
	
	public void update(BiContainer container, String action) {
		
	}
	
	private class OpenActionListener implements ActionListener {
	    private BiBrowserContainer container;

	    public OpenActionListener(BiBrowserContainer container) {
	        this.container = container;
	    }

	    public void actionPerformed(ActionEvent e) {
	    	this.container.setPath(info.getId(), info.getName());
	  	  	this.container.notify(BiBrowserContainer.PathChanged);
	    }
	}

}
