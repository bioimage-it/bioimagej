package org.francebioimaging.browser;

import org.francebioimaging.framework.BiContainer;

public class BiNavigationBarContainer extends BiContainer {

	public static String NextClicked = "BiNavigationBarContainer.NextClicked"; 
	public static String PreviousClicked = "BiNavigationBarContainer.PreviousClicked";
	public static String HomeClicked = "BiNavigationBarContainer.HomeClicked";
	
	protected String path;
	
    public BiNavigationBarContainer() {
        super();
    }
    
    public void setPath(String path) {
    	this.path = path;
    }
    
    public String getPath() {
    	return path;
    }
    
}
