package org.francebioimaging.browser;

public class BiCategory {

	public String id;
	public String name;
	public String thumbnail;
	public String parent;
	
	public BiCategory() {
		id = "";
		name = "";
		thumbnail = "";
		parent = "";
	}
	
	public void print() {
		System.out.println("id = " + id);
		System.out.println("name = " + name);
		System.out.println("thumbnail = " + thumbnail);
		System.out.println("parent = " + parent);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}
	
	
	
}
