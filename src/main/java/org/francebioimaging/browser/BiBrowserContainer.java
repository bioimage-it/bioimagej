package org.francebioimaging.browser;

import java.util.Vector;

import org.francebioimaging.framework.BiContainer;
import org.francebioimaging.process.BiProcessInfo;

public class BiBrowserContainer extends BiContainer {

	public static String ProcessesDirChanged = "BiProcessesContainer::ProcessedDirChanged";
	public static String ProcessesLoaded = "BiProcessesContainer::ProcessesLoaded";
	public static String PathChanged = "BiProcessesContainer::PathChanged";
	public static String OpenProcess = "BiProcessesContainer::OpenProcess";

	protected String categoriesJsonFile;
	protected String processesDir;
	protected Vector<BiProcessInfo> processes;
	protected Vector<BiCategory> categories;
	protected Vector<String> historyPaths;
	protected Vector<String> historyPathsNames;
	protected String currentPath;
	protected String currentPathName;
	protected int posHistory;
	protected BiProcessInfo clickedProcess;
	
	public BiBrowserContainer() {
		super();
		this.objectName = "BiExperimentContainer";
		this.processesDir = "";
		this.processes = new Vector<BiProcessInfo>();
		this.categories = new Vector<BiCategory>();
		this.historyPaths = new Vector<String>();
		this.historyPaths.add("root");
		this.historyPathsNames = new Vector<String>();
		historyPathsNames.add("Home");
		this.currentPath = "root";
		this.currentPathName = "Home";
		this.posHistory = 0;
		this.clickedProcess = null;
	}

	public void setPath(String path, String name) {
		
		this.currentPath = path;
		this.currentPathName = name;
		/*
		if ( this.posHistory < this.historyPaths.size() ) {
			for (int i = this.historyPaths.size()-1 ; i >= this.posHistory ; i--) {
				this.historyPaths.remove(i);
				this.historyPathsNames.remove(i);
			}
		}
		*/
		this.historyPaths.add(path);
		this.historyPathsNames.add(name);
		this.posHistory = this.historyPaths.size() - 1; 
	}

	public void moveToPrevious() {
		
		this.posHistory--;
		if (this.posHistory < 0 ) {
			this.posHistory = 0;
		}
		
		this.currentPath = this.historyPaths.get(this.posHistory);
		this.currentPathName = this.historyPathsNames.get(this.posHistory);
		
	}

	public void moveToNext() {
		this.posHistory += 1;
		if (this.posHistory >= this.historyPaths.size()) {
			this.posHistory = this.historyPaths.size() - 1;
		}
		this.currentPath = this.historyPaths.get(this.posHistory); 
		this.currentPathName = this.historyPathsNames.get(this.posHistory);
	}

	public void moveToHome() {
		this.setPath("root", "Home");  
	}

	public String getProcessesDir() {
		return this.processesDir;
	}
	
	public Vector<BiProcessInfo> getProcesses() {
		return this.processes;
	}
	
	public void setProcessesDir(String processesDir) {
		this.processesDir = processesDir;
	}
	
	public void setCategoriesJsonFile(String categoriesJsonFile) {
		this.categoriesJsonFile = categoriesJsonFile;
	}
	
	public String getCategoriesJsonFile() {
		return categoriesJsonFile;
	}

	public void addProcess(BiProcessInfo process) {
		this.processes.add(process);
	}

	public void setCategories(Vector<BiCategory> categories) {
		this.categories = categories;
	}

	public String getCurrentPathName() {
		return currentPathName;
	}

	public String getCurrentPath() {
		return currentPath;
	}

	public Vector<BiCategory> getCategories() {
		return categories;
	}
	
	public void setClickedProcess(BiProcessInfo process) {
		this.clickedProcess = process;
	}
	
	public BiProcessInfo getClickedProcess() {
		return clickedProcess;
	}
}
