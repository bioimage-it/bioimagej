package org.francebioimaging.runner;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.francebioimaging.process.BiProcessParameter;

public class BiProcessInputBrowser extends BiProcessInputWidget implements ActionListener {

	private static final long serialVersionUID = 1L;
	protected boolean isDir;
	protected JTextField pathEdit;
	protected JButton button;

	public BiProcessInputBrowser(BiProcessParameter input, boolean isDir) {
		super(input);
		this.isDir = isDir;
		this.pathEdit = new JTextField();
		button = new JButton("...");
		this.add(this.pathEdit);
		this.add(button);

		this.pathEdit.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void insertUpdate(DocumentEvent e) {
				warn();
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				warn();
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				warn();
			}

			public void warn() {
				value = pathEdit.getText();
				input.setValue(value);
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();

		if (source == button) {
			JFileChooser chooser = new JFileChooser();
			if (this.isDir) {
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);
			}
			int rVal = chooser.showOpenDialog(this);
			if (rVal == JFileChooser.APPROVE_OPTION) {
				this.pathEdit.setText(chooser.getSelectedFile().getName());
				//dir.setText(c.getCurrentDirectory().toString());
			}
			return;
		}
		if (source == pathEdit) {
			this.value = this.pathEdit.getText(); 
			input.setValue(this.value);
			return;
		}
	}

}
