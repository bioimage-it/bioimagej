package org.francebioimaging.runner;

import javax.swing.JPanel;

import org.francebioimaging.process.BiProcessParameter;

public class BiProcessInputWidget extends JPanel {

	private static final long serialVersionUID = 1L;
	protected String dataType;
    protected String  key;
    protected String value;
    protected boolean advanced;
    protected BiProcessParameter input;
	
	public BiProcessInputWidget(BiProcessParameter input) {
        super();
        // self.layout = QHBoxLayout()
        // self.layout.setContentsMargins(0,0,0,0)
        // self.setLayout(self.layout)
        this.dataType = "";
        this.key = "";
        this.value = "";
        this.advanced = false;
        this.input = input;
	}
	
	public BiProcessParameter getParameter() {
		return input;
	}
	
    public String getDatatype() {
        return dataType;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public boolean isAdvanced() {
        return advanced;
    }

    public void setAdvanced(boolean adv) {
        this.advanced = adv;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setDatatype(String datatype) {
        this.dataType = datatype;
    }
    
}
