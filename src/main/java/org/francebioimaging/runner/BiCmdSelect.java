package org.francebioimaging.runner;

import java.util.Vector;

public class BiCmdSelect {

	Vector<String> names;
	Vector<String> values;

	public BiCmdSelect() {
		names = new Vector<String>();
		values = new Vector<String>();
	}
	
    public int size() {
        return names.size(); 
    }

    public void add(String name, String value) {
    	names.add(name);
        values.add(value);
    }
 
}
