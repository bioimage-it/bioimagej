package org.francebioimaging.runner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BiRunnerThread implements Runnable{

	protected String command;
	protected BiRunnerContainer container;
	
	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public BiRunnerContainer getContainer() {
		return container;
	}

	public void setContainer(BiRunnerContainer container) {
		this.container = container;
	}

	@Override
	public void run() {
		Process cmdProc;
		try {
			cmdProc = Runtime.getRuntime().exec(command);

			BufferedReader stdoutReader = new BufferedReader(
					new InputStreamReader(cmdProc.getInputStream()));
			String line;
			try {
				while ((line = stdoutReader.readLine()) != null) {
					container.setStdOut(line);
					container.notify(BiRunnerContainer.NewProcessStandardOutput);
					//System.out.println("stdout:" + line);
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			BufferedReader stderrReader = new BufferedReader(
					new InputStreamReader(cmdProc.getErrorStream()));
			try {
				while ((line = stderrReader.readLine()) != null) {
					container.setStdErr(line);
					container.notify(BiRunnerContainer.NewProcessStandardError);
					//System.out.println("stderr:" + line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			int retValue = cmdProc.exitValue();
			container.setStdOut("return value: " + retValue);
			container.notify(BiRunnerContainer.NewProcessFinished);
			//int retValue = cmdProc.exitValue();
			//System.out.println("return value: " + retValue);

		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}

}
