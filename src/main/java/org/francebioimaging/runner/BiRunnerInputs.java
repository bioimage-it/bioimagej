package org.francebioimaging.runner;

import java.awt.GridLayout;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.francebioimaging.core.BiStylesheet;
import org.francebioimaging.core.BiTypes;
import org.francebioimaging.process.BiProcessInfo;
import org.francebioimaging.process.BiProcessParameter;

public class BiRunnerInputs extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected Map<String, BiProcessInputWidget > widgets;

	public BiRunnerInputs(BiProcessInfo processInfo) {
		
		BiStylesheet.defaultBackground(this);
		
		// initialize the widget
		GridLayout experimentLayout = new GridLayout(0,2);
		this.setLayout(experimentLayout);
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		
		// add inputs
		for (int i = 0 ; i < processInfo.inputsSize() ; i++) {
			BiProcessParameter input = processInfo.getInputAt(i);
			if ( !input.getType().equals(BiTypes.PARAM_HIDDEN()) && input.getIo().equals(BiTypes.IO_PARAM()) ) {
				
				JLabel label = new JLabel();
				BiStylesheet.defaultLabel(label);
				label.setText(input.getDescription());
				this.add(label);
				
				if ( input.getType().equals("integer") || input.getType().equals(BiTypes.PARAM_NUMBER()) || input.getType().equals(BiTypes.PARAM_STRING()) ){
					BiProcessInputValue valueEdit = new BiProcessInputValue(input); 
					valueEdit.setKey(input.getName());
					valueEdit.setValue(input.getValue());
					valueEdit.setAdvanced(input.isAdvanced());
                    this.add(valueEdit);
				}
                
				else if (input.getType().equals(BiTypes.PARAM_SELECT())) {
					
					String[] choices = {"default"}; 
					BiProcessInputSelect w = new BiProcessInputSelect(input, choices);
                    w.setKey(input.getName());
                    w.setSelected(input.getValue());
                    w.setAdvanced(input.isAdvanced());
                    this.add(w);
				}
        
				else if ( input.getType().equals(BiTypes.PARAM_FILE()) ){
					BiProcessInputBrowser w = new BiProcessInputBrowser(input, false);
                    w.setKey(input.getName());
                    w.setAdvanced(input.isAdvanced());
                    this.add(w);
				}
			}
		}
	}
}
