package org.francebioimaging.runner;

import org.francebioimaging.framework.BiContainer;
import org.francebioimaging.process.BiProcessInfo;
import org.scijava.ui.UIService;

import io.scif.services.DatasetIOService;
import net.imagej.display.ImageDisplayService;

public class BiRunnerContainer extends BiContainer {

	public static String ProcessFileChanged = "BiProcessEditorContainer.ProcessFileChanged";
	public static String ProcessInfoChanged = "BiProcessEditorContainer.ProcessInfoChanged";
	public static String HelpClicked = "BiProcessEditorContainer.HelpClicked";
	public static String CancelClicked = "BiProcessEditorContainer.CancelClicked";
	public static String RunClicked = "BiProcessEditorContainer.RunClicked";
	public static String MacroClicked = "BiProcessEditorContainer.MacroClicked";
	
	public static String MacroChanged = "BiProcessEditorContainer.MacroChanged";
		
	public static String NewProcessStandardOutput = "BiProcessEditorContainer.NewProcessStandardOutput";
	public static String NewProcessStandardError = "BiProcessEditorContainer.NewProcessStandardError";
	public static String NewProcessFinished = "BiProcessEditorContainer.NewProcessFinished";
	
	
	protected String processFile;
	protected BiProcessInfo processInfo;
	protected ImageDisplayService imDisp;
	protected DatasetIOService ioService;
	protected UIService uiService;
	protected String stdOut;
	protected String stdErr;
	protected String macroCommand;
	
	public BiRunnerContainer() {
		processInfo = null;
		processFile = "";
	}
	
	public BiProcessInfo getProcessInfo() {
		return processInfo;
	}
	
	public void setProcessInfo(BiProcessInfo processInfo) {
		this.processInfo = processInfo;
	}
	
	public void setProcessFile(String processFile) {
		this.processFile = processFile;
	}
	
	public String getProcessFile() {
		return processFile;
	}
	
	public void setImageDisplayService(ImageDisplayService imDisp) {
		this.imDisp = imDisp;
	}
	
	public ImageDisplayService getImageDisplayService() {
		return imDisp;
	}
	
	public void setIOService(DatasetIOService ioService) {
		this.ioService = ioService;
	}
	
	public DatasetIOService getIOService() {
		return ioService;
	}
	
	public void setUIService(UIService uiService) {
		this.uiService = uiService;
	}
	
	public UIService getUIService() {
		return uiService;
	}
	
	public String getStdOut() {
		return stdOut;
	}
	
	public void setStdOut(String stdOut) {
		this.stdOut = stdOut;
	}
	
	public String getStdErr() {
		return stdErr;
	}
	
	public void setStdErr(String stdErr) {
		this.stdErr = stdErr;
	}
	
	public String getMacroCommand() {
		return macroCommand;
	}
	
	public void setMacroCommand(String macroCommand) {
		this.macroCommand = macroCommand;
	}
}
