package org.francebioimaging.runner;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Vector;

import org.francebioimaging.core.BiTypes;
import org.francebioimaging.framework.BiContainer;
import org.francebioimaging.framework.BiModel;
import org.francebioimaging.process.BiProcessInfo;
import org.francebioimaging.process.BiProcessParameter;
import org.francebioimaging.process.BiProcessParser;

import net.imagej.Dataset;

public class BiRunnerModel extends BiModel {

	protected BiRunnerContainer container;
	protected String tmpDir;
	protected BiRunnerThread threadRunner;
	protected Thread runnerThread;

	protected Vector<String> outputsFiles;
	protected Vector<String> inputFiles;

	public BiRunnerModel(BiRunnerContainer container) {
		this.objectName = "BiRunnerModel";
		this.container = container;
		this.container.addObserver(this);

	}

	public void update(BiContainer container, String action) {
		if (action.equals(BiRunnerContainer.ProcessFileChanged)) {

			load();
			this.container.notify(BiRunnerContainer.ProcessInfoChanged);
			return;
		}

		if (action.equals(BiRunnerContainer.RunClicked)) {
			run();
			return;
		}

		if (action.equals(BiRunnerContainer.HelpClicked)) {

			String helpFileUrl = this.container.getProcessInfo().getHelp();

			File f = new File(helpFileUrl);
			if(!f.exists()) { 
				File xmlFile = new File(this.container.getProcessFile());
				String workingDir = xmlFile.getParent();
				Path fullPath = Paths.get(workingDir, helpFileUrl);
				helpFileUrl = fullPath.toString();
			}

			System.out.println("help for process " + helpFileUrl);

			if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
				try {
					Desktop.getDesktop().browse(new URI("file:///" + helpFileUrl));
				} catch (IOException | URISyntaxException e) {
					e.printStackTrace();
				}
			}
			return;
		}

		if (action.equals(BiRunnerContainer.NewProcessFinished)) {
			openOutAndFreeTmp();
			return;
		}
		
		if (action.equals(BiRunnerContainer.MacroClicked)) {
			this.container.setMacroCommand(getRunRecorderCommand());
			this.container.notify(BiRunnerContainer.MacroChanged);
			
		}
	}

	public void load() {
		BiProcessParser parser = new BiProcessParser(this.container.getProcessFile());
		BiProcessInfo info = parser.parse();
		this.container.setProcessInfo(info);
	}

	public void run() {
		System.out.println("start the process " + this.container.getProcessInfo().getName());


		// 1- Save the inputs images in tmpDir
		String inputCmd = "";
		inputFiles = new Vector<String>(); 
		Vector<BiProcessParameter> inputData = container.getProcessInfo().getInputData();
		if (inputData.size() == 1) {
			Dataset activeDataset = container.getImageDisplayService().getActiveDataset();
			try {
				String tmpFileName = Paths.get(tmpDir, "tmp_bioimagej.tif").toString();
				inputFiles.add(tmpFileName);
				System.out.println("save " + activeDataset.getName() + " to " + tmpFileName);
				container.getIOService().save(activeDataset, tmpFileName);
				inputCmd += " " + inputData.get(0).getName() + " " + tmpFileName;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else {
			System.out.println("Multiple input not yet implemented");
			return;
		}


		// 2- get parameters values from Gui
		String paramsCmd = "";
		Vector<BiProcessParameter> inputs = container.getProcessInfo().getInputs();
		for ( int i = 0 ; i < inputs.size() ; i++ ) {
			BiProcessParameter param = inputs.get(i);
			if ( ! param.isData() ) {
				paramsCmd += " " + param.getName() + " " + param.getValue();
			}
		}

		// 3- create the output files paths
		String outputCmd = "";
		outputsFiles = new Vector<String>();
		Vector<BiProcessParameter> outputs = container.getProcessInfo().getOutputs();
		for ( int i = 0 ; i < outputs.size() ; i++ ) {
			BiProcessParameter param = outputs.get(i);
			String ofileName = param.getName();
			if ( param.getType().equals(BiTypes.DATA_IMAGE())  ) {
				ofileName += "_tmp.tif";
			}
			else if ( param.getType().equals(BiTypes.DATA_TXT())  ) {
				ofileName += "_tmp.txt";
			}
			else if ( param.getType().equals(BiTypes.DATA_NUMBER()) 
					|| param.getType().equals(BiTypes.DATA_ARRAY()) 
					|| param.getType().equals(BiTypes.DATA_MATRIX()) 
					|| param.getType().equals(BiTypes.DATA_TABLE())  ) {
				ofileName += "_tmp.csv";
			}
			else {
				ofileName += "_tmp.dat";
			}

			String ofilePath = Paths.get(tmpDir, ofileName).toString();
			outputCmd += " " + param.getName() + " " + ofilePath;
			outputsFiles.add(ofilePath);
		}

		// 4- create the command line
		String cmd = container.getProcessInfo().getProgram();
		File cmdFile = new File(cmd);
		File xmlFile = new File(this.container.getProcessFile());
		String workingDir = xmlFile.getParent();
		if ( !cmdFile.exists() ) {
			Path fullPath = Paths.get(workingDir, cmd);
			cmd = fullPath.toString();
		}

		cmd += inputCmd + paramsCmd + outputCmd;
		cmd = cmd.replace("  ", " ");
		System.out.println(cmd);

		// replace the pwd variable
		cmd = cmd.replace("${pwd}", workingDir);

		// 5- Execute command line
		threadRunner = new BiRunnerThread(); 
		runnerThread = new Thread(threadRunner);
		threadRunner.setContainer(container);
		threadRunner.setCommand(cmd);
		runnerThread.start();
		//execCommand(cmd);

	}

	protected void openOutAndFreeTmp() {
		// 6- Open outputs
		for ( int i = 0 ; i < outputsFiles.size() ; i++ ) {
			System.out.println("open output file: " + outputsFiles.get(i) );
			try {
				Dataset data = container.getIOService().open(outputsFiles.get(i));
				container.getUIService().show(data);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// 7- delete temporary files
		// inputs
		for ( int i = 0 ; i < inputFiles.size() ; i++ ) {
			deleteFile(inputFiles.get(i));
		}
		// outputs
		for ( int i = 0 ; i < outputsFiles.size() ; i++ ) {
			deleteFile(outputsFiles.get(i));
		}
	}

	protected void deleteFile(String filePath) {
		File file = new File(filePath);

		if(file.delete()){
			System.out.println(file.getName() + " is deleted!");
		}else{
			System.out.println(filePath + " Delete operation is failed.");
		}
	}


	public void setTmpDir(String value) {
		this.tmpDir = value;

	}

	public String getRunRecorderCommand() {
		String command = "run(\"BiCommand\", \"xml=";
		command += container.getProcessFile();
		command += " args=[";
		command += container.getProcessInfo().getArgsCmd();
		command += "]\");";
		
		return command;
	}
}