package org.francebioimaging.runner;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;

import org.francebioimaging.core.BiStylesheet;
import org.francebioimaging.framework.BiComponent;
import org.francebioimaging.framework.BiContainer;

public class BiRunnerComponent extends BiComponent implements ActionListener{

	protected BiRunnerContainer container;
	protected JButton macroButton;
	protected JButton helpButton;
	protected JButton cancelButton;
	protected JButton okButton;
	protected JTextPane outputTextPane;
	protected JLabel runInfoLabel;
	protected JLabel outputTextLabel;
	protected JPanel runInfoWidget;
	protected JLabel runTitleLabel;
	
	public BiRunnerComponent(BiRunnerContainer container) {
		this.objectName = "BiRunnerComponent";
		this.container = container;
		container.addObserver(this);
		
		this.widget = new JPanel();
		BiStylesheet.defaultBackground(this.widget);
	}
	
	public void buildWidget() {
		
		//BoxLayout layout = new BoxLayout(this.widget, BoxLayout.Y_AXIS);
		//this.widget.setLayout(layout);
		
		GridBagLayout layout = new GridBagLayout();
		this.widget.setLayout(layout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.weighty = 5;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        
		
		JLabel titleLabel = new JLabel(this.container.getProcessInfo().getName());
		this.widget.add(titleLabel, gbc);
		BiStylesheet.pluginTitle(titleLabel);
		
		JLabel descriptionTitleLabel = new JLabel("DESCRIPTION");
		JLabel descriptionLabel = new JLabel(this.container.getProcessInfo().getDescription());
		this.widget.add(descriptionTitleLabel, gbc);
		this.widget.add(descriptionLabel, gbc);
		BiStylesheet.sectionTitle(descriptionTitleLabel);
		BiStylesheet.defaultLabel(descriptionLabel);
		
		JLabel parameterTitleLabel = new JLabel("PARAMETERS");
		this.widget.add(parameterTitleLabel, gbc);
		BiStylesheet.sectionTitle(parameterTitleLabel);
		
		
		BiRunnerInputs parametersWidget = new BiRunnerInputs(this.container.getProcessInfo());
		this.widget.add(parametersWidget, gbc);
		
		
		JPanel buttonsPanel = new JPanel();
		BiStylesheet.defaultBackground(buttonsPanel);
		
		macroButton = new JButton("Macro");
		helpButton = new JButton("Help");
		cancelButton = new JButton("Cancel");
		okButton = new JButton("Ok");
		
		macroButton.addActionListener(this);
		helpButton.addActionListener(this);
		cancelButton.addActionListener(this);
		okButton.addActionListener(this);
		
		BiStylesheet.defaultButton(macroButton);
		BiStylesheet.defaultButton(helpButton);
		BiStylesheet.defaultButton(cancelButton);
		BiStylesheet.defaultButton(okButton);
		buttonsPanel.add(macroButton);
		buttonsPanel.add(helpButton);
		buttonsPanel.add(cancelButton);
		buttonsPanel.add(okButton);
		this.widget.add(buttonsPanel, gbc);
		
		
		// run info area
		runInfoWidget = new JPanel();
        BoxLayout centralWidgetLayout = new BoxLayout(runInfoWidget, BoxLayout.Y_AXIS);
        runInfoWidget.setLayout(centralWidgetLayout);
        BiStylesheet.defaultBackground(runInfoWidget);
		
		runTitleLabel = new JLabel("RUN STATUS");
		this.widget.add(runTitleLabel, gbc);
		BiStylesheet.sectionTitle(runTitleLabel);
		
		runInfoLabel = new JLabel();
		BiStylesheet.defaultLabel(runInfoLabel);
		this.widget.add(runInfoLabel, gbc);
	
		outputTextPane = new JTextPane();
		outputTextPane.setPreferredSize(new Dimension(400,400));
	    JScrollPane sp = new JScrollPane(outputTextPane);
	    
		runInfoWidget.add(sp);
		this.widget.add(runInfoWidget, gbc);
		
		setRunAreaVisible(false);
	}
	
	public void setRunAreaVisible(boolean visible) {
		runTitleLabel.setVisible(visible);
		runInfoLabel.setVisible(visible);
		runInfoWidget.setVisible(visible);
	}
	
	public void update(BiContainer container, String action) {
		if (action.equals(BiRunnerContainer.ProcessInfoChanged)) {
			this.widget.removeAll();
			this.buildWidget();
			this.widget.validate();
			this.widget.repaint();
			return;
		}
		
		if (action.equals(BiRunnerContainer.NewProcessStandardOutput)) {
			runInfoLabel.setText("Running...");
			
			StyledDocument document = (StyledDocument) outputTextPane.getDocument();
			try {
				document.insertString(document.getLength(), "\n" + this.container.getStdOut(), null);
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
			return;
		}
		
		if (action.equals(BiRunnerContainer.NewProcessStandardError)) {
			runInfoLabel.setText("Error");
			
			StyledDocument document = (StyledDocument) outputTextPane.getDocument();
			try {
				document.insertString(document.getLength(), "\n" + this.container.getStdErr(), null);
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
			
			return;
		}
		
		if (action.equals(BiRunnerContainer.NewProcessFinished)) {
			runInfoLabel.setText("Finished");
			
			StyledDocument document = (StyledDocument) outputTextPane.getDocument();
			try {
				document.insertString(document.getLength(), "\n" + this.container.getStdOut(), null);
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
			return;
		}
		
		if (action.equals(BiRunnerContainer.MacroChanged)) {
			
			String infoMessage = this.container.getMacroCommand();
			
			JFrame frame = new JFrame();
			frame.setBounds(100,100,600,400);
			JTextPane commandTextPane = new JTextPane();
			commandTextPane.setText(infoMessage);
		    JScrollPane sp = new JScrollPane(commandTextPane);
		    frame.add(sp);
		    frame.setVisible(true);
			
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == helpButton) {
			this.container.notify(BiRunnerContainer.HelpClicked);
			return;
		}
		
		if (e.getSource() == cancelButton) {
			this.container.notify(BiRunnerContainer.CancelClicked);
			return;
		}
		
		if (e.getSource() == okButton) {
			setRunAreaVisible(true);
			this.container.notify(BiRunnerContainer.RunClicked);
			return;
		}
		
		if (e.getSource() == macroButton) {
			System.out.println("macro button clicked");
			this.container.notify(BiRunnerContainer.MacroClicked);
			return;
		}
		
	}
}
