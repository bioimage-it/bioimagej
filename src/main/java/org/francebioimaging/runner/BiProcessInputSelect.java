package org.francebioimaging.runner;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import org.francebioimaging.process.BiProcessParameter;

public class BiProcessInputSelect extends BiProcessInputWidget implements ActionListener{

	private static final long serialVersionUID = 1L;
	@SuppressWarnings("rawtypes")
	protected JComboBox combobox;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BiProcessInputSelect(BiProcessParameter input, String[] items) {
		super(input);

		combobox = new JComboBox(items); 
		combobox.addActionListener(this);
		this.add(combobox);
	}
	
	public void setSelected(String value) {
		combobox.setSelectedItem(value);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void actionPerformed(ActionEvent e) {
		
		JComboBox cb = (JComboBox)e.getSource();
		this.value = (String)cb.getSelectedItem();
		input.setValue(this.value);
	}

}
