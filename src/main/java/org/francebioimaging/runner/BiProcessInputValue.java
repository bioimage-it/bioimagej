package org.francebioimaging.runner;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.francebioimaging.core.BiStylesheet;
import org.francebioimaging.process.BiProcessParameter;

public class BiProcessInputValue extends BiProcessInputWidget implements ActionListener {


	private static final long serialVersionUID = 1L;

	protected JTextField valueEdit;

	public BiProcessInputValue(BiProcessParameter input) {
		super(input);

		BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
		this.setLayout(layout);

		this.valueEdit = new JTextField();
		BiStylesheet.defaultBackground(this);
		BiStylesheet.defaultTextField(this.valueEdit);
		this.add(this.valueEdit);
		this.valueEdit.addActionListener(this);

		this.valueEdit.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void insertUpdate(DocumentEvent e) {
				warn();
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				warn();
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				warn();
			}

			public void warn() {
				value = valueEdit.getText();
				input.setValue(value);
			}
		});
	}

	public void setValue(String value) {
		this.value = value;
		this.valueEdit.setText(this.value);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.value = this.valueEdit.getText();
		input.setValue(this.value);
	}
}
