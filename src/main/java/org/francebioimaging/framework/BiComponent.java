package org.francebioimaging.framework;

import javax.swing.JPanel;

public abstract class BiComponent extends BiObserver {
	
	protected JPanel widget;
	
    public BiComponent() {
        super();
        this.objectName = "BiComponent";   
    }

    public abstract void update(BiContainer container, String action);

    public JPanel getWidget() {
    	return widget;
    }
}
