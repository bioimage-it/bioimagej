package org.francebioimaging.framework;

abstract class BiObserver extends BiObject{

    public BiObserver() {
    	super();
        this.objectName = "BiObserver";
    }

    public abstract void update(BiContainer container, String action);

    public boolean isAction(BiContainer container, String action) {
        if ( container.getAction() == action ) {
            return true;
        }
        return false;  
    }
}
