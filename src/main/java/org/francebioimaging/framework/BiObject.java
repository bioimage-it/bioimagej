package org.francebioimaging.framework;

public class BiObject {

	protected String objectName;
	
	public BiObject() {
		this.objectName = "BiObject";
	}
	
	 public String getObjectName() {
		 return this.objectName;
	 }
}
