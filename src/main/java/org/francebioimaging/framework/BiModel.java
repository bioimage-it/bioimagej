package org.francebioimaging.framework;

public abstract class BiModel extends BiObserver {
	public BiModel() {
        super();
        this.objectName = "BiModel" ;
	}
	
	public void init() {
		
	}

	public abstract void update(BiContainer container, String action);

}
