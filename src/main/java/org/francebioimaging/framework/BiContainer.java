package org.francebioimaging.framework;

import java.util.Vector;

public class BiContainer extends BiObject {
	
	Vector<BiObserver> observers;
	String action;
	
    public BiContainer() {
    	super();
        this.objectName = "BiContainer";
        this.observers = new Vector<BiObserver>();
        this.action = "";
    }

    public void addObserver(BiObserver observer) {
        this.observers.add(observer);
    }
    
    public String getAction() {
    	return action;
    }

    public void notify(String action) {
        this.action = action;
        //printObservers();
        for (int i = 0 ; i < this.observers.size() ; i++) {
        		//System.out.println(this.objectName + " notify " + observers.get(i).getObjectName() + " with " + action);
        		observers.get(i).update(this, action);
        }
    }
    
    public void printObservers() {
    	System.out.println("Observers for " + this.objectName);
    	for (int i = 0 ; i < observers.size() ; i++) {
    		System.out.println("\t-"+observers.get(i).getObjectName());
    	}
    }
}
